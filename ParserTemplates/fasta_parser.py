#!/usr/bin/python3

import argparse

def main():

    args = parse_arguments()

    fasta_entries = get_fasta_entries(args.in_fasta)

    for entry in fasta_entries:
        print(entry)


def get_fasta_entries(fasta_path):

    """Parses single-line formatted fasta into fasta entries"""

    fasta_entries = list()

    with open(fasta_path, 'r') as in_fh:
        for line in in_fh:
            line = line.rstrip()

            header = line
            assert header.startswith(">"), print("Non-valid fasta header: {}".format(header))
            seq = next(in_fh).rstrip()
            fasta_entries.append(FastaEntry(header, seq))

    return fasta_entries


def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--in_fasta', help='Provided input fasta', required=True)
    parser.add_argument('-d', '--delimitor', help='Field delimitor for fasta headers', default=' ')
    parser.add_argument('-f', '--field_number', help='The field to merge on')
    parser.add_argument('-o', '--out_fasta', help='Parsed output fasta')
    args = parser.parse_args()
    return args

class FastaEntry:

    def __init__(self, header, sequence):
        self.header = header
        self.sequence = sequence

    def __str__(self):
        return '>{}\n{}'.format(self.header, self.sequence)

if __name__ == '__main__':
    main()


