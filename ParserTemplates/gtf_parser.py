#!/usr/bin/python3

import argparse

def main():

    args = parse_arguments()

    gtf_entries = list()
    with open(args.in_gff, 'r') as gff_fh:
        for gff_line in gff_fh:

            gtf_entry = GTFEntry(gff_line)
            gtf_entries.append(gtf_entry)


def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--in_gtf', help='Input gtf file', required=True)
    args = parser.parse_args()

    return args


class GTFEntry:

    """
    Represents a single GTF entry
    Names of columns taken from http://www.ensembl.org/info/website/upload/gff.html
    """

    def __init__(self, gtf_line):
        self.gtf_line = gtf_line
        gtf_entries = gtf_line.split('\t')

        self.seqname = gtf_entries[0]
        self.source = gtf_entries[1]
        self.feature = gtf_entries[2]
        self.start = int(gtf_entries[3])
        self.end = int(gtf_entries[4])
        self.score = gtf_entries[5]
        self.strand = gtf_entries[6]
        self.frame = gtf_entries[7]

        self.attributes_string = gtf_entries[8]
        self.attributes = self._get_attributes_dict(self.attributes_string)

    @staticmethod
    def _get_attributes_dict(attributes_string, splitter=';', attr_splitter=' ', val_trim='"'):

        """
        Takes a string with attributes, and returns a dictionary with key-value pairs
        The user is able to provide splitting characters and optionally a trimming character
        """

        attributes = [attr.strip() for attr in attributes_string.split(splitter)]
        attr_dict = dict()
        for attr in attributes[0:-1]:
            key, value = [val.strip(val_trim) for val in attr.split(attr_splitter)]
            attr_dict[key] = value
        return attr_dict

    def __str__(self):
        return self.gtf_line

if __name__ == '__main__':
    main()
