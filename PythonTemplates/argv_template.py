#!/usr/bin/python3

import sys

EXPECTED_ARGUMENTS = [sys.argv[0], 'first', 'second']
FORMAT_STRING = ' '.join(['{}'] * (len(EXPECTED_ARGUMENTS)))
EXPECTED_USAGE_STRING = FORMAT_STRING.format(*EXPECTED_ARGUMENTS)

def main():

    arg_dict = get_input_arguments()


def get_input_arguments():

    if len(sys.argv) != len(EXPECTED_ARGUMENTS):
        print('Invalid number of arguments')
        print('Expected usage: {}'.format(EXPECTED_USAGE_STRING))

    arguments = dict()
    for pos in range(1, len(EXPECTED_ARGUMENTS)):
        arguments[EXPECTED_ARGUMENTS[pos]] = sys.argv[pos]
    return arguments

if __name__ == '__main__':
    main()
