#!/usr/bin/python3

import argparse

def main():

    args = parse_arguments()


def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Provided input file')
    parser.add_argument('-o', '--output', help='Target for output file')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()


