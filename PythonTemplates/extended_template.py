#!/usr/bin/python3

version = 'v1.0.0'
release_date = 'XX-XX-XX'
author = 'Jakob Willforss (jakob.willforss@hotmail.com)'
description = '[Insert script description here]'

help_message = 'Author: {}\nRelease date: {}\nVersion: {}\n\n{}'.format(author, release_date, version, description)

import argparse

def main():

    args = parse_arguments()


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message)

    parser.add_argument('-i', '--input', help='Provided input file')
    parser.add_argument('-o', '--output', help='Target for output file')
    
    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    main()

