#!/bin/bash

usage() {
    echo "Usage: $0 -i input -o output"
    exit 1
}

while getopts ":o:i:" opt; do
    case $opt in
        i)
            input_file=$OPTARG
            ;;
        o)
            output_file=$OPTARG
            ;;
    esac
done

if [[ -z $output_file ]] || [[ -z $input_file ]]; then
    echo "Not all required parameters are set!"
    usage
fi

