#!/bin/bash

# Simple input-output script
# Options provided through two positional arguments

args=("$@")

if (($# != 2)); then
    echo "Invalid number of parameters!"
    echo "Usage: $(basename $0) input output"
    exit
fi

input=$1
output=$2

echo "Input: $input Output: $output"
